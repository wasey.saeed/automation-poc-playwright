package page;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import BaseObject.BasePageObject;
import pageObjects.HomeObject;
import pageObjects.LoginObject;

public class login extends BasePageObject {

    LoginObject loginObject;
    HomeObject homeObject;


    @BeforeClass
    public void setUp(){
        initialization();
        loginObject = new LoginObject();
        homeObject = new HomeObject();
    }

   /* @Test
    public void OpenSingInPage()
    {
        System.out.println(loginObject.validateLoginPageTitle());
        loginObject.openSignInPage();
    }*/

    @Test
    public void Valid_Login()
    {
        System.out.println(loginObject.validateLoginPageTitle());
        loginObject.openSignInDashboard();
        loginObject.enterCreds("Wasey.saeed@xanpool.com","Kobe#24#81");
        loginObject.pressSignInButton();
        homeObject.validateUserOnHomePage();

    }


    @Test
    public void Invalid_Login()
    {
        System.out.println(loginObject.validateLoginPageTitle());
        loginObject.enterCreds("","");
        loginObject.pressSignInButton();
        loginObject.validateUserLoggedIn();

    }

    @Test
    public void Invalid_LoginWithEmptyCreds()
    {
        System.out.println(loginObject.validateLoginPageTitle());
        loginObject.enterCreds(" "," ");
        loginObject.pressSignInButton();
        loginObject.validateUserLoggedIn();

    }

}
