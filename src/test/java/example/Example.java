package example;

import com.microsoft.playwright.*;
import com.microsoft.playwright.BrowserContext;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Example {
    public static void main(String[] args) {
        try (Playwright playwright = Playwright.create()) {
            Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setSlowMo(50));
            BrowserContext brContext = browser.newContext();
            Page page = brContext.newPage();
            page.navigate("https://dashboard.xanpay.com/login");

            page.locator("//input[@name=\"email\"]").fill("wasey.saeed@xanpool.com");
            page.locator("//input[@name=\"password\"]").fill("Kobe#24#81");
            page.locator("//button[@type=\"submit\"]").click();
           /*  page.fill("#email","wasey.saeed@xanpool.com");
            page.fill("#passwd","Kobe#24#81");
            page.click("#SubmitLogin");*/

            brContext.storageState(new BrowserContext.StorageStateOptions().setPath(Paths.get("firstlogin.json")));
        }
    }
}