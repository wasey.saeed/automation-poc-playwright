package pageObjects;

import BaseObject.BasePageObject;
import com.microsoft.playwright.Page;

public class HomeObject extends BasePageObject {

    Page homePage;

    public HomeObject() {homePage = page;}

    public void validateUserOnHomePage() {
        homePage.locator("//li[text()='Create checkout link']").click();

    }
}
