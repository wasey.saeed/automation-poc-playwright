package pageObjects;
import BaseObject.BasePageObject;
import com.microsoft.playwright.BrowserContext;
import com.microsoft.playwright.Page;

import static config.ConfigProperties.*;

public class LoginObject extends BasePageObject {

    static Page Login_page, SignIn;

    public LoginObject()  { SignIn = page;
    Login_page = page;}


    public void enterCreds(String email, String password)
    {
        Login_page.navigate(baseUrlForProd);
        Login_page.locator("//input[@name=\"email\"]").fill(email);
        Login_page.locator("//input[@name=\"password\"]").fill(password);
    }


    public void pressSignInButton() {
        Login_page.locator("//button[@type=\"submit\"]").click();
    }

    public String validateLoginPageTitle() {
       return Login_page.title();
    }

    public void validateUserLoggedIn() {
    }

    public void openSignInPage() {
        //Login_page.navigate("https://xanpay.com");

        SignIn.navigate(baseUrl);
        // Get page after a specific action (e.g. clicking a link)
        SignIn = context.waitForPage(() -> {
            SignIn.locator("//button[text()='Login']").click(); // Opens a new tab
        });
        SignIn.waitForLoadState();
        System.out.println(SignIn.title());
        SignIn.close();

    }

    public void openSignInDashboard() {
        Login_page.navigate(baseUrlForProd);
    }
}
