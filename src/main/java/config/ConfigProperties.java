package config;
import java.io.IOException;

public class ConfigProperties {


    public static ConfigReader appConfig = new ConfigReader();
    public static String Host = appConfig.getHost();
    public static String baseUrl = appConfig.getBaseUrl();
    public static String baseUrlForProd = appConfig.getBaseUrlForProd();

}
