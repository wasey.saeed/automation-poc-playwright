package config;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    Properties prop = new Properties();
    String fileName = "src/main/resources/Config.properties";
    {
        try {
            InputStream input = new FileInputStream(fileName);
            prop.load(input);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getHost() {
        return prop.getProperty("host");
    }

    public String getBaseUrl() {
        return prop.getProperty("BaseUrl");
    }

    public String getBaseUrlForProd(){
        return prop.getProperty("BaseUrlForProd");
    }
}