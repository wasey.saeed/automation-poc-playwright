package BaseObject;

import com.microsoft.playwright.*;



public class BasePageObject {
    public static Page page;
    static Browser browserChrome;

    // New instance for each test method.
    public static BrowserContext context;
    static Playwright playwright;

    public static void initialization() {
        playwright = Playwright.create();
        browserChrome = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false).setSlowMo(100));
        context = browserChrome.newContext();
        page = context.newPage();
    }

    public void quitDriver()
    {
        context.close();
        playwright.close();
    }
}